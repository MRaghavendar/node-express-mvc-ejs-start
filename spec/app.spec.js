//jasmine provides a function called 'describe'
//arg 1 - description 
//arg 2 - function (logic)

var request = require('request')

const base_url = 'https://localhost:8089'

describe('Test app.js', () => {
    describe('GET /', () => {
        it('returns a status code of 200', () => {
            //request call back contains 3 args
            request.get(base_url, (error, response, body) => {
                expect(response.statusCode).toBe(200);
                done();
            })
        })
    })
})

describe('Test app.js', () => {
    describe('GET /about', () => {
        it('returns a status code of 200', () => {
            //request call back contains 3 args
            request.get(base_url, (error, response, body) => {
                expect(response.statusCode).toBe(200);
                done();
            })
        })
    })
})

describe('Test app.js', () => {
    describe('GET /category', () => {
        it('returns a status code of 200', () => {
            //request call back contains 3 args
            request.get(base_url, (error, response, body) => {
                expect(response.statusCode).toBe(200);
                done();
            })
        })
    })
})

describe('Test app.js', () => {
    describe('GET /account', () => {
        it('returns a status code of 200', () => {
            //request call back contains 3 args
            request.get(base_url, (error, response, body) => {
                expect(response.statusCode).toBe(200);
                done();
            })
        })
    })
})

describe('Test app.js', () => {
    describe('GET /transaction', () => {
        it('returns a status code of 200', () => {
            //request call back contains 3 args
            request.get(base_url, (error, response, body) => {
                expect(response.statusCode).toBe(200);
                done();
            })
        })
    })
})

describe('Test app.js', () => {
    describe('GET /organization', () => {
        it('returns a status code of 200', () => {
            //request call back contains 3 args
            request.get(base_url, (error, response, body) => {
                expect(response.statusCode).toBe(200);
                done();
            })
        })
    })
})

describe('Test app.js', () => {
    describe('GET /ilovewebapps', () => {
        it('returns a status code of 404', () => {
            //request call back contains 3 args
            request.get(base_url, (error, response, body) => {
                expect(response.statusCode).toBe(404);
                done();
            })
        })
    })
})