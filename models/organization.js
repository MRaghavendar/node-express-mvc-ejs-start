/** 
*  Organization model
*  Describes the characteristics of each attribute in a Organization resource.
*
* @author Raghavendar Reddy Maddelavedu <S534692@nwmissouri.edu>
*
*/

// see <https://mongoosejs.com/> for more information
const mongoose = require('mongoose')

const OrganizationSchema = new mongoose.Schema({
  _id: { type: Number, required: true },
  email: { type: String, required: true, unique: true },
  organizationName: { type: String, required: true, default: 'Given' },
  street1: { type: String, required: true, default: '' },
  street2: { type: String, required: false, default: '' },
  city: { type: String, required: true, default: '' },
  state: { type: String, required: true, default: '' },
  zip: { type: String, required: true, default: '' },
  country: { type: String, required: true, default: '' }
})

module.exports = mongoose.model('organization', OrganizationSchema)
