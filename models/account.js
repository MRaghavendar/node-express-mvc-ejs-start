/** 
*  account model
*  Describes the characteristics of each attribute in a account resource.
*
* @author Nikitha Lakmarapu<s534690@nwmissouri.edu>
*
*/

// see <https://mongoosejs.com/> for more information
const mongoose = require('mongoose')

const AccountSchema = new mongoose.Schema({
  _id: { type: String, required: true },
  given: { type: String, required: true, default: 'Given' },
  family: { type: String, required: true, default: 'Family' },
  date_created:{type:String,required: true},
  phone_no:{type: Number, required: true },
  email: { type: String, required: true, unique: true },
  street1: { type: String, required: true, default: 'Street 1' },
  street2: { type: String, required: false, default: '' },
  city: { type: String, required: true, default: 'Maryville' },
  state: { type: String, required: true, default: 'MO' },
  zip: { type: Number, required: true, default: '64468' },
  country: { type: String, required: true, default: 'USA' }
})

module.exports = mongoose.model('account', AccountSchema)
