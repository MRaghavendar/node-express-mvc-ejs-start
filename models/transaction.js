/** 
*  account model
*  Describes the characteristics of each attribute in a account resource.
*
* @author Pathuri Bhavani<s534690@nwmissouri.edu>
*
*/

// see <https://mongoosejs.com/> for more information
const mongoose = require('mongoose')



const TransactionSchema = new mongoose.Schema({

  _id: { type: Number, required: true },
  transactionDate: { type: String, required: true },
  transactionType: { type: String, required: true},
  transactionAmount: {type: Number, required:true},
})



module.exports = mongoose.model('Transaction', TransactionSchema)
