// set up a temporary (in memory) database
const Datastore = require('nedb')
const LOG = require('../utils/logger.js')

// require each data file
// Decalaring the constant variables for data location
const accounts = require('../data/accounts.json')
const organizations = require('../data/organizations.json')
const categories = require('../data/categories.json')
const transactions = require('../data/transactions.json')

// inject the app to seed the data
module.exports = (app) => {
  LOG.info('START seeder.')
  const db = {}

  // Categories Data
  //insert the sample data into categories
  db.categories = new Datastore()
  db.categories.loadDatabase()

  // insert the sample data into our data store
  db.categories.insert(categories)

  // initialize app.locals (these objects will be available to our controllers)
  app.locals.categories = db.categories.find(categories)
  LOG.debug(`${app.locals.categories.query.length} categories seeded`)


  // Accounts Data
  //insert the sample data into accounts
  db.accounts = new Datastore()
  db.accounts.loadDatabase()

  // insert the sample data into our data store
  db.accounts.insert(accounts)

  // initialize app.locals (these objects will be available to our controllers)
  app.locals.accounts = db.accounts.find(accounts)
  LOG.debug(`${app.locals.accounts.query.length} accounts seeded`)

  // Transactions Data
  //insert the sample data into transaction
  db.transactions = new Datastore()
  db.transactions.loadDatabase()

  // insert the sample data into our data store
  db.transactions.insert(transactions)

  // initialize app.locals (these objects will be available to our controllers)
  app.locals.transactions = db.transactions.find(transactions)
  LOG.debug(`${app.locals.transactions.query.length} transactions seeded`)

  // Organizations Data
  //insert the sample data into organizations
  db.organizations = new Datastore()
  db.organizations.loadDatabase()

  // insert the sample data into our data store
  db.organizations.insert(organizations)

  // initialize app.locals (these objects will be available to our controllers)
  app.locals.organizations = db.organizations.find(organizations)
  LOG.debug(`${app.locals.organizations.query.length} organizations seeded`)

  LOG.info('END Seeder. Sample data read and verified.')
}
